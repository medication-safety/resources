from PyPDF2 import PdfFileWriter, PdfFileReader

# merges pocket_card_template.pdf
# with logos/logo_front.pdf and logos/logo_back.pdf
# into pocket_card.pdf

output = PdfFileWriter()

template = PdfFileReader(open('pocket_card_template.pdf', 'rb'))
front_stamp = PdfFileReader(open('logos/logo_front.pdf', 'rb')).getPage(0)
back_stamp = PdfFileReader(open('logos/logo_back.pdf', 'rb')).getPage(0)

page = template.getPage(0)
page.mergePage(front_stamp)
output.addPage(page)

page = template.getPage(1)
page.mergePage(back_stamp)
output.addPage(page)

with open('pocket_card.pdf', 'wb') as f:
	output.write(f)
