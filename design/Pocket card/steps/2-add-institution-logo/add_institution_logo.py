from PyPDF2 import PdfFileWriter, PdfFileReader

# merges pocket_card_template.pdf
# with logos/muw.pdf
# into pocket_card.pdf

output = PdfFileWriter()

template = PdfFileReader(open('pocket_card_template.pdf', 'rb'))
logo_stamp = PdfFileReader(open('logos/muw.pdf', 'rb')).getPage(0)

page = template.getPage(0)
page.mergePage(logo_stamp)
output.addPage(page)

output.addPage(template.getPage(1))

with open('pocket_card.pdf', 'wb') as f:
	output.write(f)
